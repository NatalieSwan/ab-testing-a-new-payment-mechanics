# A/B testing. New payment mechanics
The project represents a test assignment of a company.   
During A/B testing of one hypothesis, the target group was offered a new payment mechanics on the site, while the control group was left with the basic mechanics.  

## Project objective
The task is to analyze the results of the experiment and conclude whether it is worth launching the new payment mechanics for all users.

## Stack
![Python](https://img.shields.io/badge/python-3670A0?style=for-the-badge&logo=python&logoColor=ffdd54)
* pandas, numpy, pingouin, scipy.stats, matplotlib, seaborn, 
* requests, urllib.parse - for loading datasets via API
  
* EDA
* methods of statistical analysis: Chi-square (Pearson's criterion), Student’s t-test

## Datasets
We have 4 csv-files:

[groups.csv](https://disk.yandex.ru/d/hm0_kjhT0CzJEg) - a file with information about whether the user belongs to the control (A) or experimental (B) group

**Table. Sample dataset Groups**

id      |	grp
--------|-------
1259471 |	B
1885870 |	A

[groups_add.csv](https://disk.yandex.ru/d/MqENg60tvLfqWg) - additional file with users, which was sent 2 days after the data transfer

[active_studs.csv](https://disk.yandex.ru/d/T3azl2umujrTGw) - a file with information about users who logged in to the platform during the days of the experiment 

**Table. Sample dataset active_studs**

student_id |
-----------|   
   581585  |
   5723133 |

[checks.csv](https://disk.yandex.ru/d/NLkjlBiycCQjQg) - a file with information about users' payments during the days of the experiment. 

**Table. Sample dataset checks**

student_id | rev
-----------|-------
1659434	   | 690.0
5004439	   | 1900.0
5250795	   | 199.0

## Project Implementation:
The code of the project you can see in the file [AB test new payment mechanics.ipynb]()

* I used API to upload datasets.
* I performed preliminary analysis and preprocessing of the data.
* I used the concat method to merge the first two datasets.
* I used the merge method to merge the remaining datasets.
* Since the assignment did not specify the goal of changing the payment mechanics, I chose three metrics for comparison in groups, which can be used to understand whether the company will achieve the three goals in case of innovation:

### 1. The goal is to increase conversion
If the new mechanics are intended to make the payment process more convenient in order to increase the conversion rate of this part of the funnel, then we just need to see if the percentage of people who paid has changed and if this change is statistically significant.

**Table. The percentage paid in each group**

grp | number of students who have paid | share |
----|----------------------------------|-------|
A |	78 |	5.07 |
B |	314 |	4.62 |

We can see that the new mechanics may have worsened conversion, but is the difference really statistically significant?

I used Chi-square to answer because conversion is a proportional metric, 'bought-not-bought' are categorical variables. Pearson's chi-squared test will tell us if there is a relationship between group and purchase conversion.

**The conditions for applying the test are met:**

* All observations are independent.
* The number of observations is more than 20 (Some sources say that there should be at least 50 observations. In our case there are more).
* The expected frequency is more than 10.

**Hypotheses**  
**H0:** there is no correlation between conversion to purchase and group membership - conversion in both groups is the same.  
**H1:** conversion in groups is statistically significantly different (there is a correlation between group membership and purchase conversion).

**Conclusion**
р-value > 0.05, so we cannot reject the null hypothesis: we cannot say that there is a correlation between group membership and conversion, so we cannot say that the observed decrease in the share of customers in the target group is statistically significant. This means that the new mechanics did not worsen (and did not improve) conversion to payment.

### 2. The goal is to increase the average check of a paying user
In this case, I compared ARPPU (average revenue per paying user).

**Table. ARPPU**

grp | ARPPU |
----|-------|
A | 933.589744 |
B |	1257.878992 |

The mean in the test group is higher. But is this change statistically significant?
To see distribution I built a histplot. 

![revenue distribution](revenue distribution)


The distribution is close to exponential, except that this outlier around 2000 in Group B breaks it.  
ARPPU in the target group is higher. Probably because 41% of the customers in group B made a payment of 1,900 (and 1,900,0001), while the most frequent payment amount in group A is 290. From the preliminary analysis, we know that 30% of all buyers paid 1,900 - and all 30% are in group B. 

Since the distributions are not normal, to the homogeneity test, I applied Levene's test.
The homogeneity requirements are met (р-value > 0.05). Therefore, despite the fact that the distribution is non-normal, I applied t-test, because  the sample population is larger than 30 samples, the Student's criterion works well. 

Actually, based on the central limit theorem, the distribution of the general population need not be normal - because the averages of samples from it always tend toward normality. And if I understand Student correctly, he is precisely suggesting that samples should be considered normal by default for his test, rather than applying it only to samples with a normal distribution, because we cannot know what distribution the general population to which our samples belong has.

**Hypotheses**  
**H0:** both samples are taken from the same population, averages are equal.  
**H1:** groups belong to different general populations, the averages differ statistically significantly.

**Conclusion:**
p-val< 0.05 - the null hypothesis is rejected, we can say that ARPPU differ statistically significantly.

### 3. The goal is to increase revenue from traffic with new mechanics
If the goal is to increase revenue from traffic, we should look at ARPU (Average revenue per user).

**Table. ARPU in the target group is more**

grp | ARPU |
----|------|
A |	47.347204 |
B |	58.058798 |

Again I applyed Levene's test. The homogeneity requirements are met. Therefore I used t-test.

**Hypotheses:**  
**H0:** both samples are taken from the same population, averages are equal  
**H1:** groups are taken from different populations, the averages differ statistically significantly.

**Conclusion**
p-val > 0.05 - We cannot reject the null hypothesis, we cannot state that ARPU differ statistically significantly and that the new mechanics increased revenue from traffic.

## Total conclusions
Of the analyzed metrics, only ARPPU in the target group is statistically significantly higher. Therefore, if the goal of the update was to increase the average revenue per paying user, the result is positive. And since the observed decrease in the share of buyers in the target group is not statistically significant, I can recommend running the new mechanics on all users.

If the goal of the experiment was to improve the other 2 parameters, then we do not observe statistically significant changes in them, and therefore I recommend to look for other ways to increase conversions and average revenue per user.

I also recommend you to pay attention to the fact that 40% of the buyers from group B made a payment of 1,900. Meanwhile, the control group has no such payment amount at all. Did the new mechanics assume that they would pay this amount? If so, then the task is complete and yields a good result. If that was not the design, I would look to see what that amount comes from, whether it is due to the updated mechanics or if it is an overlay of other activities. Can we use this information to increase conversion to purchase?


## My contacts
If you have any questions or suggestions for collaboration, please let me know

<div id="badges">
  <a href="https://t.me/Natalie_L_Swan">
    <img src="https://img.shields.io/badge/Telegram-blue?style=for-the-badge&logo=Telegram&logoColor=white" alt="Telegram Badge"/>
  </a>
    </div>

<p align='center'>
   📫 <a href='mailto:tasha.l.swan@gmail.com'>tasha.l.swan@gmail.com</a>
</p>



